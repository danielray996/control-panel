<?php
session_start();

require('connection.php');


if(empty($_SESSION['email'])) {
        $sql = 'SELECT * FROM utenti';
        $query = $db->prepare($sql);
        if ($query->execute()) {
            $dati = $query->fetchAll(PDO::FETCH_ASSOC);
            $trovato = false;
            for ($i = 0; $i < count($dati); $i++) {
                if ($_POST['email'] == $dati[$i]['email'] && $_POST['password'] == $dati[$i]['pass']) {
                    $trovato = true;
                    $_SESSION['email'] = $_POST['email']; 
                }
            }
            if (!$trovato) {
                ?>
                <script>
                    location.href="index.php?error=true"
                </script>
                <?php
            }else{
                ?>
                <script>
                    location.href="cp_admin.php?page=1"
                </script>
                <?php
            }
        }
}else{
    include('views/header.html');
    function filterElements($string, $db){
            $sql = $string;
            $form = '<form class="d-flex" action="cp_admin.php" method="get">
                <input class="form-control me-2" type="search" placeholder="..." aria-label="Search" name="email">
                <button class="btn btn-outline-success" type="submit">Cerca</button>
                </form>';
            $alert_div = '</div></div><div class="container" id="alert"></div>';
            $email = isset($_GET['email']) == false ? '' : $_GET['email'];
            $query = $db->prepare($sql);
            $per_page = 2;
            $query->execute();
            $tot_user= $query->rowCount();
            $tot_page = ceil($tot_user/$per_page);
            if(!isset($_GET['page'])){
                $page = 1;
                }else{
                $page = $_GET['page'];
                }
            $inizio = ($page - 1) * $per_page;
            $sql_pagination = isset($_GET['email']) == false ? "SELECT * FROM utenti LIMIT $inizio, $per_page" : "SELECT * FROM utenti  WHERE email LIKE '%$email%'";
            $query_pag = $db->query($sql_pagination);
            $dati_pag = $query_pag->fetchAll(PDO::FETCH_ASSOC);
                echo '<div class="container mt-5"><table class="table"><thead><tr>';
                if(!empty($dati_pag)){
                    foreach($dati_pag[0] as $name => $value){
                        ?><th scope="col"><?php echo strtoupper($name)?></th><?php
                    }
                    echo '<th scope="col">EDIT</th>';
                    echo '</tr></tbody>';
                    for($i = 0; $i < count($dati_pag); $i++){
                        echo '<tr>';
                        foreach($dati_pag[$i] as $name => $value){
                            ?><td><?php echo $value?></td><?php
                        }
                        echo '<td><a href="viewdata.php?id='.$dati_pag[$i]['id'].'" class="btn btn-secondary">Visualizza dati &nbsp;<i class="fas fa-database"></i></a></td>';
                        echo '</tr>';
                    }
                    echo '</tbody></thead></table><div class="navbar"><div class="d-flex">';
                    for($k = 1; $k <= $tot_page; $k++){
                        echo '<a class="btn btn-success" href="?page=' . $k . '">'.$k.'</a>&nbsp;';
                    }
                    echo '</div>';
                    echo $form;
                    echo $alert_div;
                }else{
                    echo '<div class="row">';
                    echo $form;
                    echo $alert_div;?>
                    <script type="module">
                        import { setAlert } from './js/alert.js';
                        setAlert('Nessun risultato', 'red');
                    </script>
                    <?php
                }
                
     }

     if(isset($_GET['email'])){
         filterElements('SELECT * FROM utenti  WHERE email LIKE "%'.$_GET['email'].'%"', $db);
     }else{
         filterElements('SELECT * FROM utenti', $db);
     }

     if(!empty($_GET['delete_success'])){
        ?>
        <script type="module">
                import { setAlert } from './js/alert.js';
                setAlert('<i class="fas fa-user-times"></i> Utente <b>eliminato</b> con successo!', 'green');
        </script>
        <?php
     }else if(!empty($_GET['update_success'])){
        ?>
        <script type="module">
                import { setAlert } from './js/alert.js';
                setAlert('<i class="fas fa-user-edit"></i> Utente <b>modificato</b> con successo!', 'gold');
        </script>
        <?php
     }
        
     include('views/footer.html');
}
?>


