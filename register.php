<?php include('views/header.html') ?>

    <div class="container mt-5">
        <form method="post" action="register.php">
            <div class="mb-3 navbar">
                <h4><i id="history" class="fas fa-arrow-circle-left text-success"></i></h4>
                <h4 class="text-center"><i class="fas fa-user-check"></i> Registrazione</h4>
            </div>
            <div class="mb-3">
                <label for="exampleInputName" class="form-label">Nome</label>
                <input type="text" class="form-control" id="exampleInputName" aria-describedby="emailHelp" name="nome" required>
            </div>
            <div class="mb-3">
                <label for="exampleInputSurname" class="form-label">Cognome</label>
                <input type="text" class="form-control" id="exampleInputSurname" aria-describedby="emailHelp" name="cognome" required>
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" required>
                <div class="container" id="alert"></div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" required>
            </div>
            <div class="mb-3">
                <label for="sex" class="form-label">Sesso</label>
                <select id="sex" name="sesso" class="form-control col-12">
                        <option value="">--Seleziona--</option>
                        <option value="m">M</option>
                        <option value="f">F</option>
                </select>
            </div>
            <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1" name="consenso" required>
                <label class="form-check-label" for="exampleCheck1">Acconsento</label>
            </div>
            <button type="submit" class="btn btn-primary">Invia</button>
        </form>
    </div>

    <!-- Validator Form with JS Vanilla -->
    <script type="module">
         import { setAlert } from './js/alert.js';

            var email = document.getElementById('exampleInputEmail1');
            var histor = document.getElementById('history');
            histor.style.cursor = 'pointer';
            histor.addEventListener('click', () => {
                window.history.back();
            })

            document.querySelector('button').addEventListener('click', (e) => {
                if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value))
                {  
                    e.preventDefault()
                    setAlert('<i class="fas fa-exclamation-triangle"></i> Email non valida! - Formato corretto: email@cpanel.it', 'red')
                }
            })
    </script>

    <!-- End validator form -->

    <!-- PHP Request to INSERT INTO cpanel database users data -->
    <?php

        require('connection.php');

        $dati = $_POST;
        if(!empty($dati)){
            $sql = 'INSERT INTO utenti(nome, cognome, pass, email, sesso, consenso) VALUES(:nome,:cognome,:pass, :email, :sesso, :consenso)';
            $query = $db->prepare($sql);
            $elementsBind = [
                0 => ['param' => ':nome', 'userInp' => $dati['nome'], 'strControl' => PDO::PARAM_STR],
                1 => ['param' => ':cognome', 'userInp' => $dati['cognome'], 'strControl' => PDO::PARAM_STR],
                2 => ['param' => ':pass', 'userInp' => $dati['password'], 'strControl' => PDO::PARAM_STR],
                3 => ['param' => ':email', 'userInp' => $dati['email'], 'strControl' => PDO::PARAM_STR],
                4 => ['param' => ':sesso', 'userInp' => $dati['sesso'], 'strControl' => PDO::PARAM_STR_CHAR],
                5 => ['param' => ':consenso', 'userInp' => $dati['consenso'], 'strControl' => PDO::PARAM_INT]
            ];

            for($cont = 0; $cont < count($elementsBind); $cont++){
                $query->bindParam($elementsBind[$cont]['param'], $elementsBind[$cont]['userInp'], $elementsBind[$cont]['strControl']);
            }
            
            if($query->execute()){
                ?>
                <script>location.href="index.php?register=true"</script>
                <?php
            }else{
                ?>
                <script>location.href="index.php?error=true"</script>
                <?php
            }
        }
    ?>
    <!-- End PHP Request -->

<?php include('views/footer.html') ?>