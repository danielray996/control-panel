export function setAlert(message, color){
        var alert = document.createElement('p');
                    document.getElementById('alert').appendChild(alert);
                    alert.innerHTML = message;
                    alert.style.fontSize = '1.5rem';
                    alert.classList.add('text-center', 'mt-5', 'py-2');
                    alert.style.color = 'white';
                    alert.style.textShadow = '2px 2px 2px black'
                    alert.style.backgroundColor = color;
                    alert.style.borderRadius = '3px';
                    
                setTimeout(function(){ 
                    alert.style.display = 'none';
                }, 3000);
        }