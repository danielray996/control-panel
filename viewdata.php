<?php 
    include('views/header.html');
    
    require('connection.php');

    $sql = 'SELECT * FROM utenti WHERE id='.$_GET['id'];
        $query = $db->prepare($sql);
        if ($query->execute()) {
            $dati = $query->fetch(PDO::FETCH_ASSOC);
        }
?>

    <div class="container mt-5">
        <form method="post" action="update.php?id=<?php echo $_GET['id']?>";>
            <div class="mb-3 navbar">
                <h4><i id="history" class="fas fa-arrow-circle-left text-success"></i></h4>
                <h4 class="text-center"><i class="fas fa-user-edit"></i> Dati utente</h4>
            </div>
            <div class="mb-3">
                <label for="exampleInputName" class="form-label">Nome</label>
                <input type="text" class="form-control" id="exampleInputName" aria-describedby="emailHelp" name="nome" value="<?php echo $dati['nome']?>" required>
            </div>
            <div class="mb-3">
                <label for="exampleInputSurname" class="form-label">Cognome</label>
                <input type="text" class="form-control" id="exampleInputSurname" aria-describedby="emailHelp" name="cognome" value="<?php echo $dati['cognome']?>" required>
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="<?php echo $dati['email']?>" required>
                <div id="emailError" class="form-text d-none"></div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name="password" value="<?php echo $dati['pass']?>" required>
            </div>
            <div class="mb-3">
                <label for="sex" class="form-label">Sesso</label>
                <select id="sex" name="sesso" class="form-control col-12">
                        <option value="">--Seleziona--</option>
                        <option value="m">M</option>
                        <option value="f">F</option>
                </select>
            </div>
            <div id="buttons" class="mb-3">
            <input type="submit" class="btn btn-warning text-white" value="Modifica">
            <a href="delete.php?id=<?php echo $dati['id']?>" type="submit" class="btn btn-danger">Elimina</a>
            </div>
        </form>
    </div>

<script>
    document.getElementById('buttons').style.textAlign = 'center';
    var histor = document.getElementById('history');
    histor.style.cursor = 'pointer';
    histor.addEventListener('click', () => {
        window.history.back();
    })
</script>