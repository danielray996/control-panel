<?php 
session_start();
if(empty($_SESSION['email']) || isset($_GET['logout'])){?>
    <?php include('views/header.html');
    session_unset();
    ?>

    <div class="container mt-5">
        <form action="cp_admin.php?page=1" method="post">
            <div class="row">
                <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label"><i class="fas fa-envelope-open-text"></i> Email</label>
                <input id="exampleInputEmail1 type="email" class="col-12 form-control" placeholder="Email" name="email">
                </div>
                <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label"><i class="fas fa-key"></i> Password</label>
                <input for="exampleInputPassword1" type="password" class="col-12 form-control" placeholder="Password" name="password">
                </div>
                <div id="credentialError" class="form-text d-none"></div>
                <div class="mt-3" id="buttons">
                    <input type='submit' class="btn btn-secondary" value='Login'>
                    <button class="btn btn-primary">
                        Registrati
                    </button>
                </div>
            </div>
        </form>

    </div>
    <div class="container" id="alert"></div>
    <?php include('views/footer.html');?>
<?php 
}else{
   ?>
   <script>location.href="cp_admin.php";</script>
   <?php
}
?>

<!-- DOM module elements and redirect - JS -->
<script type="module">
    var url = location.href;
    url = url.split("index.php");
 
    import { setAlert } from './js/alert.js';

    var button = document.querySelector('button')
    var orientation = document.getElementById('buttons')
    var exit = document.getElementById('exit')

    exit.style.display = 'none';
    orientation.style.textAlign = 'right';

    button.addEventListener('click', function(e) {
        e.preventDefault()
        location.href = url[0]+'register.php';
    })

    if(location.href == url[0]+"index.php?error=true"){
                setAlert('Credenziali <b>non valide</b>!', 'red')
    }else if(location.href == url[0]+"index.php?logout=true"){
                setAlert('Hai effettuato il <b>logout</b> con successo!', 'green')
    }else if(location.href == url[0]+"index.php?register=true"){
                setAlert('Hai effettuato la <b>registrazione</b> con successo! Accedi subito!', 'green')
    }
</script>


